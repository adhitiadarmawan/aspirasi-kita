<?php include("inc/header-aspirasik.php") ?>
	<!-- MAIN HERO PANEL -->
	<div id="hero-panel">
		<div class="overlay">
			<div class="inner">
				<h2>SUSUN KABINET IMPIANMU</h2>
				<p>Siapa sih sebenarnya orang-orang yang menurutmu cocok menjadi menteri? Ikut bermain dan susun kabinet impianmu untuk Indonesia.</p>
				
				<form class="aspirasik-form" action="aspirasik-wapres.php" method="post">
					<select class="dark-selector">
						<option>PILIH PRESIDEN</option>
						<option>Joko Widodo</option>
						<option>Soeharto</option>
						<option>Gusdur</option>
					</select>

					<input type="submit" value="Susun Kabinet" class="btn" />
				</form>
			</div>
		</div>
	</div>
    <!-- /MAIN HERO PANEL -->
<?php include("inc/footer.php") ?>