<?php include("inc/header.php") ?>
<div class="container">
	<div class="row">
		<div class="col-md-8 col-centered">
			<div class="gap"></div>
				<div class="page-title text-center">
					<h1>BANGUN ASPIRASI</h1>
					<p>Terimakasih telah bergabung dengan Aspirasikita.org dan kesediaanmu menyuarakan aspirasi di sini.
Kami percaya, perubahan positif diawali dari kemauan menuangkan pemikiran dan berdiskusi.
Mulai suarakan aspirasimu!</p>

				<form action="" method="POST" id="submit-aspirasi">
					<div class="field">
						<input type="text" name="judul" id="judul" placeholder="Judul" required/>
					</div>
					<div class="field">
						<select id="kategori" name="kategori" required>
							<option>Kategori</option>
						</select>
					</div>
					<div class="field">
						<input type="text" name="tag" id="tag" placeholder="tag. Cth: Politik, Kabinet, etc" required/>
					</div>
					<div id="polling" class="field text-left">
						<input type="checkbox" name="check" id="apakah-polling"/><label for="apakah-polling">Apakah ini polling?</label>
						<div class="polling-input" style="display:none;">
							<input type="text" name="pilihan-1" placeholder="Pilihan 1"/>
							<input type="text" name="pilihan-2" placeholder="Pilihan 2"/>
						</div>
					</div>
					<div class="field">
						<input type="file"/>
					</div>
					<div class="field">
						<input type="text" name="url-video" placeholder="URL Video" required>
					</div>
					<div class="field">
						<textarea name="aspirasi" required>ASPIRASI</textarea>
					</div>
					<input type="submit" class="btn" value="Kirim Aspirasi">
				</form>
				</div>
		</div>
	</div>
</div>
	
<?php include("inc/footer.php") ?>