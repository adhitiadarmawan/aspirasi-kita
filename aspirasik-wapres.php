<?php include("inc/header-aspirasik.php") ?>
	<div class="aspirasik step-2">
		<div class="container">
			<div class="row">
				 <div class="col-lg-12">
				 	<div class="gap"></div>
				 	<div class="text-center">
						<img src="assets/img/step-2.png">
					</div>
					<div class="page-title">
						<h1>PILIH WAKIL PRESIDEN</h1>
						<p>Siapa wakil presiden yang cocok mendampingi presidenmu?</p>
						<form action="aspirasik-kabinet.php" method="post">
							<div class="photo-container">
								<img src="assets/img/null-pic.png">
							</div>
							<div class="col-md-4 col-centered">
								<select style="width:100%">
									<option>Wakil Presiden</option>
									<option>Calon 1</option>
									<option>Calon 2</option>
								</select>
							</div>
							<div class="gap"></div>
							<div class="text-center">
								<a href="" class="btn dark">Kembali</a> 
								<input type="submit" value="Selanjutnya" class="btn">
							</div>
							<div class="gap"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include("inc/footer.php") ?>