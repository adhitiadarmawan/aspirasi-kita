<?php include("inc/header.php") ?>
<div class="bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-centered">
				<div class="gap"></div>
				<div id="user-profile">
					<div class="col-md-6">
						<div class="user-picture fade-down" style="background-image:url(http://pickaface.net/includes/themes/clean/img/slide2.png)"></div>
						<div class="user-details col-md-7">
							<h1 class="user-name fade-down">Sebut saja mawar</h1>
							<p class="user-since fade-down">Member since August 2014</p>

							<div class="aspirasi-data post fade-up">
								<div class="counter">10</div>
								<p>Aspirasi</p>
							</div>

							<div class="aspirasi-data comment fade-up">
								<div class="counter">11</div>
								<p>Komentar</p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<img src="assets/img/user-chart.png">
					</div>
				</div>
				<div class="gap clearfix"></div>
			</div>
		</div>
	</div>
</div>
<div class="bg-white">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-centered">
				<div class="gap"></div>
				<div id="myTab" role="tabpanel">
					  <!-- Nav tabs -->
					  <div class="text-center">
						  <ul class="nav nav-tabs" role="tablist">
						    <li role="presentation" class="active"><a href="#aspirasi" aria-controls="aspirasi" role="tab" data-toggle="tab">Aspirasi</a></li>
						    <li role="presentation"><a href="#komentar" aria-controls="komentar" role="tab" data-toggle="tab">Komentar</a></li>
						  </ul>
					  </div>

					  <!-- Tab panes -->
					  <div class="tab-content">
					    <div role="tabpanel" class="tab-pane active" id="aspirasi">
					    	<ul class="blog-items">
				             	<?php for ($i = 0; $i < 8; $i++):?>
				                <li class="blog-item col-md-3 col-sm-3 col-xs-6 fade-up">
				                	<a href="single-page.php">
					                    <div class="item-inner"  style="background-image:url(https://unsplash.it/380/380?random)">
					                    	<div class="item-icon-wrapper"><i class="pe-7s-close"></i></div>
					                        <div class="overlay">
					                        	<h2>Metropolis</h2>
					                        	<p>Nascetur ridiculus mus. Donec quam felis, ultricies nec nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.</p>
					                        	<div class="overlay-metas">
					                        		<ul class="list-inline">
				                        				<li><a href="#"><i class="fa fa-share"></i> 10</a></li>
													</ul>
						                            <a class="post-date hidden-xs" href="#">8 Mei 2014</a> 
						                            <a class="view-post" href="#"><i class="fa fa-comments"></i> 30</a></a>       
					                            </div>      
					                        </div>           
					                    </div>
				                    </a>        
				                </li><!--/.blog-item-->
				                <?php endfor;?>
				            </ul>
					    </div>
					    <div role="tabpanel" class="tab-pane" id="komentar">
					    	<ul class="blog-items">
				             	<?php for ($i = 0; $i < 8; $i++):?>
				                <li class="blog-item col-md-3 col-sm-3 col-xs-6 fade-up">
				                	<a href="single-page.php">
					                    <div class="item-inner"  style="background-image:url(https://unsplash.it/380/380?random)">
					                    	<div class="item-icon-wrapper"><i class="pe-7s-close"></i></div>
					                        <div class="overlay">
					                        	<h2>BANJIR JAKARTA</h2>
					                        	<p>Nascetur ridiculus mus. Donec quam felis, ultricies nec nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.</p>
					                        	<div class="overlay-metas">
					                        		<ul class="list-inline">
				                        				<li><a href="#"><i class="fa fa-share"></i> 10</a></li>
													</ul>
						                            <a class="post-date hidden-xs" href="#">8 Mei 2014</a> 
						                            <a class="view-post" href="#"><i class="fa fa-comments"></i> 30</a></a>       
					                            </div>      
					                        </div>           
					                    </div>
				                    </a>        
				                </li><!--/.blog-item-->
				                <?php endfor;?>
				            </ul>
					    </div>
					  </div>

					</div>
					
		            <div class="gap clearfix"></div>
			</div>
		</div>
	</div>
</div>
<?php include("inc/footer.php") ?>