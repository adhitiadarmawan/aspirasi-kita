<?php include("inc/header-aspirasik.php") ?>
	<div class="aspirasik step-4">
		<div class="container">
			<div class="row">
				 <div class="col-lg-12">
				 	<div class="gap"></div>
				 	<div class="text-center">
						<img src="assets/img/step-4.png">
					</div>
					<div class="page-title">
						<h1>KABINET TERKIRIM</h1>
						<p>Terima kasih atas kiriman kamu. Kabinet susunanmu akan langsung terlihat di galeri.<br>Sampai bertemu di permainan berikutnya!</p>
						<div class="gap"></div>
						<a href="aspirasik.php" class="btn dark">Kembali</a>
					</div>
				</div>
			</div>
			<div class="gap"></div>
		</div>
	</div>
<?php include("inc/footer.php") ?>