<?php include("inc/header.php") ?>
    <!-- MAIN HERO PANEL -->
	<div id="hero-panel">
		<div class="overlay">
			<div class="inner">
				<h2>Suarakan Aspirasimu</h2>
				<p>Hanya kamu yang bisa bangun harapan kita. Berbagi aspirasi, berikan komentar, dan mulai diskusi dengan teman-temanmu</p>
				<a href="#" class="btn">MULAI BERBAGI</a>
			</div>
			<div class="box">
				<h3 class="counter">25</h3>
				<h4>ASPIRASI BERTOPIK <strong>PELAYANAN PUBLIK</strong> TERBANGUN HARI INI</h4>
				<a href="#" class="btn">LIHAT DETAIL</a>
			</div>
		</div>
	</div>
    <!-- /MAIN HERO PANEL -->

    <!-- Page Content -->

    <div class="content-section">        

		<section id="hot-topics">
			<div class="container">
			<div class="col-xs-12 col-sm-6 col-md-9">
	    		<div class="gap"></div> 
	        	<div class="gap fade-down section-heading">
	                <h2 class="main-title"><strong>Aspirasi</strong> Terbaru</h2>
	            </div> 

	            <div class="row gap">
		            <ul class="blog-items">
		            	<!--ITEM WITH THUMBNAIL-->
		       			<li class="blog-item col-md-4 col-sm-3 col-xs-6 fade-up with-thumbnail">
		                	<a href="single-page.php">
			                    <div class="item-inner" style="background-image:url(https://unsplash.it/380/380?random)">
			                    	<div class="item-icon-wrapper"><i class="pe-7s-close"></i></div>
			                        <div class="overlay">
			                        	<span class="category">Kebutuhan Publik</span>
			                        	<h2>Siapa yang sesungguhnya memenangkan debat capres - cawapres kemarin?</h2>
			                        	<div class="overlay-metas">
			                        		<ul class="list-inline">
		                        				<li><a href="#"><i class="fa fa-share"></i> 10</a></li>
											</ul>
				                            <a class="post-date hidden-xs" href="#">8 Mei 2014</a> 
				                            <a class="view-post" href="#"><i class="fa fa-comments"></i> 30</a></a>       
			                            </div>      
			                        </div>           
			                    </div>
		                    </a>        
		                </li><!--/.blog-item-->
		             	<?php for ($i = 0; $i < 2; $i++):?>
		             	<!--ITEM WITHOUT THUMBNAIL-->
		                <li class="blog-item col-md-4 col-sm-3 col-xs-6 fade-up no-thumbnail">
		                	<a href="single-page.php">
			                    <div class="item-inner">
			                    	<div class="item-icon-wrapper"><i class="pe-7s-close"></i></div>
			                        <div class="overlay">
			                        	<span class="category">PENEGAKKAN HUKUM &amp; TATA KELOLA NEGARA</span>
			                        	<h2>Gerakan Digital Kembali ke Ekonomi Warga</h2>
			                        	<p>Nascetur ridiculus mus. Donec quam felis, ultricies nec nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.</p>
			                        	<div class="overlay-metas">
			                        		<ul class="list-inline">
		                        				<li><a href="#"><i class="fa fa-share"></i> 10</a></li>
											</ul>
				                            <a class="post-date hidden-xs" href="#">8 Mei 2014</a> 
				                            <a class="view-post" href="#"><i class="fa fa-comments"></i> 30</a></a>       
			                            </div>      
			                        </div>           
			                    </div>
		                    </a>        
		                </li><!--/.blog-item-->
		                <?php endfor;?>
		            </ul>
	            </div>
	         </div>
	         <div id="featured-post" class="col-xs-12 col-sm-6 col-md-3">
		         <div >
		         	<h3 class="section-title">Aspirasi <strong>Populer</strong></h3>
		         	<div class="carousel">
		         		<div class="item" style="background-image:url(https://unsplash.it/380/380?random=3)">
		         			<div class="overlay description">
		         				<h4>Siapakah yang lebih unggul dalam debat Capres dengan tema Pembangunan Ekonomi &amp; Kesejahteraan Sosial?</h4>
		         			</div>
		         			<div class="pagination"></div>
		         		</div>
		         	</div>
		         </div>	
	         </div>

	    </section>

	    <section id="photography">
			<div id="photography-carousel" class=" slide full-width-carousel carousel-fade">
                <div class="carousel-inner">
                    <div class="item active">
                        <img src="https://unsplash.it/1200/450?image=534" alt="First slide">
                        <div class="container">
	                        <div class="carousel-caption fade-up">
	                        	<div class="col-md-4 col-md-offset-4">
						        	<div class="centered gap section-heading">
						                <h2 class="main-title">Kabinet Negara</h2>
						                <p>Melalui game yang asik, Aspirasimu juga akan di perdengarkan. Pilih dan susun kabinet negara impianmu!</p>
										<a href="aspirasik.php" class="btn">Main Sekarang</a>   
						            </div> 
	                            </div>
	                        </div>
                        </div>
                    </div>
            </div>
	    </section>

    <div class="content-section">        


		<section id="bottom-section">
			<div class="container">
			<div class="gap"></div> 
			<div id="chart-widget" class="col-xs-12 col-sm-6 col-md-3 fade-up">
	    		<h3>KABAR <strong>ASPIRASI</strong></h3>
	    		<select>
	    			<option value="">Kategori</option>
	    			<option>Pendidikan</option>
	    			<option>Kesehatan</option>
	    			<option>Air &amp; Pangan</option>
	    			<option>Transportasi</option>
	    		</select>
	    		<div id="pie-chart" style="width: 300px; height: 300px;">
	    			<img src="assets/img/chart-sample.png">
	    		</div>
	    	</div>

			<div id="other-topics" class="col-xs-12 col-sm-6 col-md-9">
	            <div class="row gap">
		            <ul class="blog-items">
		            	<?php for ($a = 0; $a < 3; $a++):?>
		            	<!--ITEM WITH THUMBNAIL-->
		       			<li class="blog-item col-md-4 col-sm-3 col-xs-6 fade-up with-thumbnail">
		                	<a href="single-page.php">
			                    <div class="item-inner" style="background-image:url(https://unsplash.it/380/380?random)">
			                    	<div class="item-icon-wrapper"><i class="pe-7s-close"></i></div>
			                        <div class="overlay">
			                        	<span class="category">Kebutuhan Publik</span>
			                        	<h2>Siapa yang sesungguhnya memenangkan debat capres - cawapres kemarin?</h2>
			                        	<div class="overlay-metas">
			                        		<ul class="list-inline">
		                        				<li><a href="#"><i class="fa fa-share"></i> 10</a></li>
											</ul>
				                            <a class="post-date hidden-xs" href="#">8 Mei 2014</a> 
				                            <a class="view-post" href="#"><i class="fa fa-comments"></i> 30</a></a>       
			                            </div>      
			                        </div>           
			                    </div>
		                    </a>        
		                </li><!--/.blog-item-->
		                <?php endfor;?>
		             	<?php for ($i = 0; $i < 3; $i++):?>
		                <li class="blog-item col-md-4 col-sm-3 col-xs-6 fade-up no-thumbnail">
		                    <div class="item-inner">
		                    	<div class="item-icon-wrapper"><i class="pe-7s-close"></i></div>
		                        <div class="overlay">
		                        	<span class="category">Kebutuhan Publik</span>
		                        	<h2>Metropolis</h2>
		                        	<p>Nascetur ridiculus mus. Donec quam felis, ultricies nec nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.</p>
		                        	<div class="overlay-metas">
		                        		<ul class="list-inline">
	                        				<li><a href="#"><i class="fa fa-share"></i> 10</a></li>
										</ul>
			                            <a class="post-date hidden-xs" href="#">8 Mei 2014</a> 
			                            <a class="view-post" href="#"><i class="fa fa-comments"></i> 30</a></a>       
		                            </div>      
		                        </div>           
		                    </div>        
		                </li><!--/.blog-item-->
		                <?php endfor;?>
		            </ul>
		            <div id="pagination" class="gap fade-up text-center">							
						<a href="#" class="btn">Selanjutnya</a>
					</div>
	            </div>
	         </div>
	    </section>

<?php include("inc/footer.php") ?>