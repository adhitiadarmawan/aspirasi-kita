<?php include("inc/header-aspirasik.php") ?>
	<div class="aspirasik profil">
		<div class="container">
			<div class="row">
				 <div class="col-lg-11 col-centered">
				 	<div class="gap"></div>
					<div class="page-title">
						<h1>POLITIKUS POPULER</h1>
						<p>Lihat politikus-politikus yang bisa kamu jadikan referensi dalam bermain game AspirAsik</p>
						<div class="gap"></div>
						<ul class="profil-politikus">
							<?php for ($i = 0; $i < 20; $i++):?>
							<li class="col-md-3 fade-up">
								<a href="aspirasik-profil-single.php">
								<div class="photo" style="background-image:url(assets/img/politikus.jpg)"></div>
								<h3>Rizal Sukma</h3>
								</a>
								<div class="gap"></div>
							</li>
							<?php endfor;?>
						</ul>
						<div class="text-center">
							<a href="" class="btn">Selanjutnya</a> 
						</div>
						<div class="gap"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include("inc/footer.php") ?>