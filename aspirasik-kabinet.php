<?php include("inc/header-aspirasik.php") ?>
	<div class="aspirasik step-3">
		<div class="container">
			<div class="row">
				 <div class="col-lg-12">
				 	<div class="gap"></div>
				 	<div class="text-center">
						<img src="assets/img/step-3.png">
					</div>
					<div class="page-title">
						<h1>SUSUN KABINET</h1>
						<p>Susun kabinet kerja yang pas untuk pasangan presiden dan wakil presidenmu</p>
						<div class="gap"></div>
						<form action="aspirasik-finish.php" method="post">
							<div class="col-md-6 col-centered">
								<div class="col-md-6">
									<div class="photo-container">
										<img src="assets/img/null-pic.png">
									</div>
									<h4>Presiden</h4>
									<select style="width:100%">
										<option>Presiden</option>
										<option>Calon 1</option>
										<option>Calon 2</option>
									</select>
								</div>
								<div class="col-md-6">
									<div class="photo-container">
										<img src="assets/img/null-pic.png">
									</div>
									<h4>Wakil Presiden</h4>
									<select style="width:100%">
										<option>Wakil Presiden</option>
										<option>Calon 1</option>
										<option>Calon 2</option>
									</select>
								</div>
							</div>
							<div class="gap clearfix"></div>
							<div class="menteri-container">
								<?php for ($i = 0; $i < 32; $i++):?>
									<div class="menteri-box col-md-3">
										<div class="photo-container">
											<img src="assets/img/null-pic.png">
										</div>
										<h4>Menteri Sosial</h4>
										<select style="width:100%">
											<option>Pilih Menteri</option>
											<option>Calon 1</option>
											<option>Calon 2</option>
										</select>
									</div>
								<?php endfor;?>
							</div>
							<div class="gap clearfix"></div>
							<div class="text-center">
								<a href="" class="btn dark">Kembali</a> 
								<input type="submit" value="Selanjutnya" class="btn">
							</div>
							<div class="gap"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include("inc/footer.php") ?>