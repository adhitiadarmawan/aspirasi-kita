<?php include("inc/header-aspirasik.php") ?>
	<div class="aspirasik gallery">
		<div class="container">
			<div class="row">
				 <div class="col-lg-12 col-centered">
				 	<div class="gap"></div>
					<div class="page-title">
						<h1>GALERI</h1>
						<h2>KABINET NEGARA</h2>
						<div class="gap"></div>
						<ul class="gallery-kabinet">
							<?php for ($i = 0; $i < 20; $i++):?>
							<li class="col-md-3 fade-up">
								<a class="box" href="aspirasik-profil-single.php">
									<h3>KABINET KERJA INDONESIA RAYA</h3>
									<div class="meta-info">
										<small>OLEH</small>
										<p class="author">ADHITIA DARMAWAN</p>
										<span class="region">Indonesia</span>
									</div>
									<div class="date">
										9 Nov 2014
									</div>
								</a>
							</li>
							<?php endfor;?>
						</ul>
						<div class="gap"></div>
						<div class="text-center clearfix">
							<a href="" class="btn">Selanjutnya</a> 
						</div>
						<div class="gap"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php include("inc/footer.php") ?>