<?php include("inc/header.php") ?>

    <!-- MAIN SLIDER -->
	<div id="single-slider">
		<script type="text/javascript">
			$(document).ready(function(){
			  	jQuery('#single-slider').backstretch([
			      "http://unsplash.it/1200/400?random=1",
			    ], {duration: 8000, fade: 500});
		    });
		</script>
		<div class="single-slider-post-heading">		
			<div class="container">
	        	<div class="centered gap fade-down section-heading">
	        		<p><span class="category text-uppercase"><a href="#">Kebutuhan publik</a></span></p>
	                <h2 class="main-title"><strong>Siapa yang sesungguhnya memenangkan debat capres - cawapres kemarin?</strong></h2>
	            </div> 
			</div>
		</div>
	</div>
    <!-- /MAIN SLIDER -->

	<!-- Main Content
	================================================== -->
    <div class="content-section">        

		<section>
			<div class="container">
				<div class="gap"></div>
				<div class="row">
					<div class="col-md-8 col-centered">
						<div class="row gap">
							<div id="single-page" class="col-md-12 fade-up single-post-content">
								<article>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>

									<h2>So Why Choose Lorem Ipsum?</h2>

									<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.</p>

									<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p> 

								</div>
							</article>
						</div>


						
						<div class="gap fade-down section-heading post-section-heading">
			                <h2 class="main-title"><strong>3 Komentar</strong></h2>
			            </div>

		                <div id="comments">
		                    <div id="comments-list">
		                        <div class="media">
		                            <div class="pull-left fade-right">
		                                <img class="avatar comment-avatar" src="assets/img/Comments/comment_images_03.jpg" alt="">
		                            </div>
		                            <div class="media-body fade-left">
		                                <div class="well">
		                                    <div class="media-heading">
		                                        <strong>Dave Evans</strong>
		                                        <small>30th Jan, 2014</small>
		                                    </div>
		                                    <p>Was are delightful solicitude discovered collecting man day. Resolving neglected sir tolerably but existence conveying for. Day his put off unaffected literature partiality inhabiting.</p>
		                                    <a class="" href="#">Reply</a>
		                                </div>
		                                <div class="media">
		                                    <div class="pull-left">
		                                        <img class="avatar comment-avatar" src="assets/img/Comments/comment_images_07.jpg" alt="">
		                                    </div>
		                                    <div class="media-body">
		                                        <div class="well">
		                                            <div class="media-heading">
		                                                <strong>Peter Jackson</strong>&nbsp; <small>16th Jan, 2014</small>
		                                            </div>
		                                            <p>Wicket longer admire do barton vanity itself do in it. Preferred to sportsmen it engrossed listening. Park gate sell they west hard for the. Abode stuff noisy manor blush yet the far. Up colonel so between removed so do.</p>
		                                            <a class="" href="#">Reply</a>
		                                        </div>
		                                    </div>
		                                </div><!--/.media-->
		                            </div>
		                        </div><!--/.media-->
		                        <div class="media">
		                            <div class="pull-left fade-right">
		                                <img class="avatar comment-avatar" src="assets/img/Comments/comment_images_10.jpg" alt="">
		                            </div>
		                            <div class="media-body fade-left">
		                                <div class="well">
		                                    <div class="media-heading">
		                                        <strong>John Smith</strong>&nbsp; <small>14th Jan, 2014</small>
		                                    </div>
		                                    <p>Quitting informed concerns can men now. Projection to or up conviction uncommonly delightful continuing. In appetite ecstatic opinions hastened by handsome admitted.</p>
		                                    <a class="" href="#">Reply</a>
		                                </div>
		                            </div>
		                        </div><!--/.media-->
		                    </div><!--/#comments-list--> 

							<div class="gap fade-down section-heading post-section-heading">
				                <h2 class="main-title"><strong>Komentar Anda</strong></h2>
				            </div>

	                    	<div id="comment-form" class="mt fade-up ">
		                        <form class="form-horizontal" role="form">
                                    <textarea rows="8" class="form-control" placeholder="Pesan kamu"></textarea>
                                	<button type="submit" class="btn">Kirim komentar</button>                           
		                        </form>
	                    	</div><!--/#comment-form-->
		                </div><!--/#comments-->   
	                </div>

                </div>
			</div>
		</section>

    </div>


<?php include("inc/footer.php") ?>