<?php include("inc/header.php") ?>

	<!-- Main Content
	================================================== -->
    <div class="content-section">        

		<section>
			<div class="container">
	            	<div class="page-title text-center fade-down">
						<h1>Aspirasi</h1>
						 <h2 class="main-title">Kebutuhan <strong>Publik</strong></h2>
					</div>

	            <div class="row gap">
					<ul class="blog-items">
		            	<?php for ($a = 0; $a < 6; $a++):?>
		            	<!--ITEM WITH THUMBNAIL-->
		       			<li class="blog-item col-md-3 col-sm-3 col-xs-6 fade-up with-thumbnail">
		                	<a href="single-page.php">
			                    <div class="item-inner" style="background-image:url(https://unsplash.it/380/380?random)">
			                    	<div class="item-icon-wrapper"><i class="pe-7s-close"></i></div>
			                        <div class="overlay">
			                        	<span class="category">Kebutuhan Publik</span>
			                        	<h2>Siapa yang sesungguhnya memenangkan debat capres - cawapres kemarin?</h2>
			                        	<div class="overlay-metas">
			                        		<ul class="list-inline">
		                        				<li><a href="#"><i class="fa fa-share"></i> 10</a></li>
											</ul>
				                            <a class="post-date hidden-xs" href="#">8 Mei 2014</a> 
				                            <a class="view-post" href="#"><i class="fa fa-comments"></i> 30</a></a>       
			                            </div>      
			                        </div>           
			                    </div>
		                    </a>        
		                </li><!--/.blog-item-->
		                <?php endfor;?>
		             	<?php for ($i = 0; $i < 6; $i++):?>
		                <li class="blog-item col-md-3 col-sm-3 col-xs-6 fade-up no-thumbnail">
		                    <div class="item-inner">
		                    	<div class="item-icon-wrapper"><i class="pe-7s-close"></i></div>
		                        <div class="overlay">
		                        	<span class="category">Kebutuhan Publik</span>
		                        	<h2>Metropolis</h2>
		                        	<p>Nascetur ridiculus mus. Donec quam felis, ultricies nec nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.</p>
		                        	<div class="overlay-metas">
		                        		<ul class="list-inline">
	                        				<li><a href="#"><i class="fa fa-share"></i> 10</a></li>
										</ul>
			                            <a class="post-date hidden-xs" href="#">8 Mei 2014</a> 
			                            <a class="view-post" href="#"><i class="fa fa-comments"></i> 30</a></a>       
		                            </div>      
		                        </div>           
		                    </div>        
		                </li><!--/.blog-item-->
		                <?php endfor;?>
		            </ul>
	            </div>

				<div id="pagination" class="gap fade-up text-center">							
					<a href="#" class="btn">Selanjutnya</a>
				</div>
			</div>
		</section>

    </div>


<?php include("inc/footer.php") ?>
