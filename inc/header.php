<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Aspirasi Kita</title>

    <!-- CSS -->
    <link href="assets/css/style.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="assets/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- jQuery -->
 	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body >
<!-- Navigation -->
    <nav class="navbar yamm navbar-default isStuck" role="navigation" id="main-navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navigation-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php"><img src="assets/img/logo.png" alt="Logo" /></a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="main-navigation-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="aspirasik.php">Aspir<strong>asik</strong></a></li>
                    <li><a href="category-archive.php">Aspirasi</a></li>
                    <li><a href="static-page.php">Tentang Aspirasikita</a></li>
                    <li><a href="#">Data Aspirasi</a></li>
                </ul>
                  <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="button"><i class="fa fa-search"></i></button>
                  </form>
                  <ul class="nav navbar-nav navbar-right">
                    <li><a href="submit.php" class="btn"><img src="assets/img/icon-bgnaspirasi.png" alt="icon"> Bangun Aspirasi</a></li>
                    <li class="user-menu"><a href="#"><img src="assets/img/user-icon.png" alt="Login"></a></li>
                  </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <?php include 'carousel-menu.php' ?>
    <?php include 'user-panel.php'; ?>
    <div id="content-wrap">
