    <footer class="footer-main">
        <div class="container">
            <div class="row">
                 <div class="col-lg-12 fade-up">
                     <a class="col-md-3" href="index.php"><img src="assets/img/logo.png" alt="Logo" /></a>
                     <div id="footer-nav">
                        <div class="col-md-5">
                             <h3>ASPIRASI</h3>
                             <ul class="footer-menu col-md-4">
                                 <li><a href="">Kebutuhan Publik</a></li>
                                 <li><a href="">Lingungan Energi</a></li>
                                 <li><a href="">Lainnya</a></li>
                             </ul>
                             <ul class="footer-menu col-md-8">
                                 <li><a href="">Penegakkan Hukum &amp; Tata Kelola Negara</a></li>
                                 <li><a href="">Konflik Sosial</a></li>
                                 <li><a href="">Hak Asasi Manusia</a></li>
                             </ul>
                         </div>
                         <div class="col-md-4">
                             <ul class="footer-menu-secondary col-md-5">
                                 <li><a href="">ASPIRASIK</a></li>
                                 <li><a href="">Tentang Kami</a></li>
                                 <li><a href="">Data Aspirasi</a></li>
                             </ul>
                            <ul class="footer-menu-secondary col-md-3">
                                 <li><a href="">Donasi</a></li>
                                 <li><a href="">Kontak</a></li>
                                 <li><a href="">Privasi</a></li>
                            </ul>
                            <ul class="footer-menu-secondary col-md-4">
                                 <li><a href="">Karir</a></li>
                                 <li><a href="">Mitra</a></li>
                            </ul>
                         </div>
                     </div> 
                 </div>
            </div>
        </div>
    </footer>

    <!-- Footer -->
    <footer class="footer-secondary">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 fade-up">
                        <ul class="social-widget list-inline col-md-3">
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>                    
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>   
                        </ul>
                    <p class="copyright col-md-6 text-center">Copyright <?php echo date("Y") ?> aspirasikta.org. All right reserved</p>
                    <p id="back-to-top" class="col-md-3 text-right">Back to top <a href="#"><i class="fa fa-angle-up"></i></a></p>
                </div>
            </div>
        </div>
    </footer>
</div><!--end content wrap-->
    <!-- JS Files -->   
    <script src="assets/js/plugins.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>    
    <script src="assets/js/init.js"></script>

</body>

</html>