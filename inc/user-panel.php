    <div id="user-panel" class="text-center">
        <div class="container">
            <div class="row">
                <div id="page-register">
                    <form class="register-form">
                        <div class="step-1">
                            <h2 class="fade-down">BERGABUNG DENGAN ASPIRASI<strong>KITA</strong></h2>
                            <ul class="login-social" class="fade-up">
                                <li class="facebook"><a href="#" class="btn"><i class="fa fa-facebook"></i> Connect with Facebook</a></li>
                                <li class="twitter"><a href="#" class="btn"><i class="fa fa-twitter"></i> Connect with Twitter</a></li>
                                <li class="gplus"><a href="#" class="btn"><i class="fa fa-google-plus"></i> Connect with Google</a></li>
                            </ul>
                            <h3 class="fade-down">Atau isi form registrasi</h3>
                            
                            <div class="col-md-7 col-centered fade-up">
                                <div class="field"><input type="text" placeholder="NAMA LENGKAP"/></div>
                                <div class="field"><input type="email" placeholder="EMAIL"/></div>
                                <div class="field"><input type="password" placeholder="PASSWORD"/></div>
                                <div class="field"><a class="btn next">Selanjutnya</a></div>

                                <strong>Sudah memiliki account? <a href="#page-forgot" class="login-trigger">Sign In</a></strong>
                            </div>
                        </div>
                        <div class="step-2" style="display:none;">
                            <h2 class="fade-down">LENGKAPI DATA KAMU</h2>
                            <h3 class="fade-down">Atau isi form registrasi</h3>
                            <div class="col-md-7 col-centered fade-up">
                                <div class="field"><input type="text" placeholder="TANGGAL LAHIR"/></div>
                                <div class="field"><input type="text" placeholder="JENIS KELAMIN"/></div>
                                <div class="field"><input type="text" placeholder="KOTA/PROVINSI"/></div>
                                <div class="field"><input type="submit" class="btn" value="DAFTAR"/></div>

                            </div>
                        </div>
                    </form>
                    <div class="step-3" style="display:none;">
                            <h2 class="fade-down">TERIMA KASIH</h2>                           
                            <div class="col-md-7 col-centered fade-up">
                                <p>Terima kasih atas pendaftaran diri kamu di AspirasiKita.<br>Mulai jelajahi topik-topik aspirasi menarik dan ikut berdiskusi bersama member lainnya.</p>
                                <a href="" class="btn">Jelajahi Aspirasi</a>
                            </div>
                        </div>
                </div>

                <div id="page-login" class="login" style="display:none;">
                    <h2 class="fade-down">SIGN IN KE ASPIRASI<strong>KITA</strong></h2>
                    <form class="login-form" action="dashboard.php">
                        <ul class="login-social" class="fade-up">
                            <li class="facebook"><a href="#" class="btn"><i class="fa fa-facebook"></i> Connect with Facebook</a></li>
                            <li class="twitter"><a href="#" class="btn"><i class="fa fa-twitter"></i> Connect with Twitter</a></li>
                            <li class="gplus"><a href="#" class="btn"><i class="fa fa-google-plus"></i> Connect with Google</a></li>
                        </ul>
                        <h3 class="fade-down">Atau Sign in dengan email</h3>
                        <div class="col-md-7 col-centered fade-up">
                            <div class="field"><input type="email" placeholder="EMAIL"/></div>
                            <div class="field"><input type="password" placeholder="PASSWORD"/></div>
                            <div class="field"><input type="submit" class="btn" value="MASUK" /></div>

                            <a href="#page-forgot" class="forgot-trigger">Lupa Password?</a>
                        </div>
                    </form>
                </div>
                <div id="page-forgot" class="forgot" style="display:none;">
                    <h2 class="fade-down">LUPA PASSWORD</h2>
                    <div class="col-md-6 col-centered fade-up">
                       <p>Tuliskan email yang kamu gunakan untuk mendaftar di AspirasiKita.<br>Kami akan mengirimkan password reset ke email kamu.</p>
                        <form class="forgot-form">
                            <div class="field"><input type="email" placeholder="EMAIL" autofocus/></div>
                            <div class="field"><input type="submit" class="btn" value="SUBMIT" /></div>
                        </form>
                   </div>
                </div>
            </div>
        </div>
    </div>